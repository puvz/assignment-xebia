FROM jenkins/jenkins:2.289.1-lts-jdk11
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV JENKINS_USER admin
ENV JENKINS_PASS admin
COPY role.yml "/usr/share/jenkins/role.yml"
COPY default-user.groovy "/usr/share/jenkins/ref/init.groovy.d/"
ENV CASC_JENKINS_CONFIG "/usr/share/jenkins/role.yml"
RUN jenkins-plugin-cli --plugins configuration-as-code:1.51 role-strategy:3.1.1